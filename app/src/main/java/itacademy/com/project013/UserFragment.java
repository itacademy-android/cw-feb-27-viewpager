package itacademy.com.project013;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class UserFragment extends Fragment {

    private ArrayList<TabPagerItem> mTabs = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);

        createTabs();

        init(view);

        return view;
    }

    private void init(View view) {
        ViewPager pager = view.findViewById(R.id.viewPager);
        pager.setAdapter(new MyPagerStateAdapter(getActivity().getSupportFragmentManager(), mTabs));
        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(pager);
    }

    private void createTabs() {
        mTabs.add(new TabPagerItem(new FirstFragment(), "First Fragment"));
        mTabs.add(new TabPagerItem(new SecondFragment(), "Second Fragment"));
    }
}
